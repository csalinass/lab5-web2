<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        return view('welcome_message');
    }
    public function mostrarPerritos()
    {
        // Cargar la vista perritos_view.php
        return view('perritos_view');
    }
}
